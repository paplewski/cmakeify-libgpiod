#if defined(LIBGPIOD_VER_2)

#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <gpiod.hpp>
#include <iostream>
#include <thread>

namespace {

/* Example configuration - customize to suit your situation. */
const ::std::filesystem::path chip_path("/dev/gpiochip1");
const ::gpiod::line::offset line_offset = 3;

::gpiod::line::value toggle_value(::gpiod::line::value v)
{
    return (v == ::gpiod::line::value::ACTIVE) ? ::gpiod::line::value::INACTIVE : ::gpiod::line::value::ACTIVE;
}

} /* namespace */

int main()
{
    std::cout << "USING LIBGPIOD V2!" << ::std::endl;
    ::gpiod::line::value value = ::gpiod::line::value::ACTIVE;
    std::cout << chip_path << std::endl;
    auto request =
        ::gpiod::chip(chip_path)
            .prepare_request()
            .set_consumer("toggle-line-value")
            .add_line_settings(line_offset, ::gpiod::line_settings().set_direction(::gpiod::line::direction::OUTPUT))
            .do_request();

    for (;;) {
        ::std::cout << line_offset << "=" << value << ::std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));
        value = toggle_value(value);
        request.set_value(line_offset, value);
    }
}

#else

#include <filesystem>
#include <gpiod.hpp>
#include <iostream>
#include <thread>

const ::std::filesystem::path chip_path("/dev/gpiochip1");
unsigned int line_offset = 3;

int main(int argc, char** argv)
{
    ::std::cout << "USING LIBGPIOD V1!" << ::std::endl;
    std::cout << chip_path << std::endl;

    ::gpiod::chip chip(chip_path);
    auto line = chip.get_line(line_offset);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ::std::cout << line_offset << "=" << 1 << ::std::endl;
    line.request({argv[0], ::gpiod::line_request::DIRECTION_OUTPUT, 0}, 1);

    for (;;) {
        ::std::cout << line_offset << "=" << 0 << ::std::endl;
        line.set_value(0);
        std::this_thread::sleep_for(std::chrono::seconds(1));
        ::std::cout << line_offset << "=" << 1 << ::std::endl;
        line.set_value(1);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return EXIT_SUCCESS;
}
#endif
